/* SPDX-License-Identifier: GPL-3.0-or-later */

#include "test_multicodec.hpp"
#include "ipfs-tiny/multiformats/multicodec.hpp"
#include "ipfs-tiny/multiformats/varint.hpp"
#include <random>

using namespace ipfs_tiny::multiformats;

void
test_multicodec::test_state()
{
  const size_t                  msg_len = 1024;
  etl::vector<uint8_t, msg_len> res;

  multicodec m;
  CPPUNIT_ASSERT(!m.valid());
  CPPUNIT_ASSERT_THROW(m.encode(res), multicodec_invalid_state);

  m.set_code(multicodec::raw);
  CPPUNIT_ASSERT(m.valid());
  CPPUNIT_ASSERT_NO_THROW(m.encode(res));

  m.set_code(multicodec::dag_pb);
  CPPUNIT_ASSERT(m.valid());
  CPPUNIT_ASSERT_NO_THROW(m.encode(res));
}

void
test_multicodec::test_different()
{
  const size_t                  msg_len = 1024;
  etl::vector<uint8_t, msg_len> res;

  multicodec raw_m(multicodec::raw);
  multicodec dag_pb_m(multicodec::dag_pb);

  res = raw_m.encode(res);
  dag_pb_m.decode(res.cbegin(), res.cend());
  CPPUNIT_ASSERT(raw_m == dag_pb_m);
}

void
test_multicodec::test_raw()
{
  const size_t                  msg_len = 1024;
  etl::vector<uint8_t, msg_len> res;

  multicodec m0(multicodec::raw);
  multicodec m1;

  res = m0.encode(res);
  m1.set_code(multicodec::raw);
  m1.decode(res.cbegin(), res.cend());
  CPPUNIT_ASSERT(m0 == m1);
}

void
test_multicodec::test_dag_pb()
{
  const size_t                  msg_len = 1024;
  etl::vector<uint8_t, msg_len> res;

  multicodec m0(multicodec::dag_pb);
  multicodec m1;

  res = m0.encode(res);
  m1.set_code(multicodec::dag_pb);
  m1.decode(res.cbegin(), res.cend());
  CPPUNIT_ASSERT(m0 == m1);
}

void
test_multicodec::test_static_raw()
{
  const size_t                  msg_len = 1024;
  etl::vector<uint8_t, msg_len> res;

  codec c0 = multicodec::encode<multicodec::raw>(res);
  codec c1 = multicodec::decode<multicodec::raw>(res.cbegin(), res.cend());
  CPPUNIT_ASSERT(c0 == c1);
}

void
test_multicodec::test_static_dag_pb()
{
  const size_t                  msg_len = 1024;
  etl::vector<uint8_t, msg_len> res;

  codec c0 = multicodec::encode<multicodec::dag_pb>(res);
  codec c1 = multicodec::decode<multicodec::dag_pb>(res.cbegin(), res.cend());
  CPPUNIT_ASSERT(c0 == c1);
}
