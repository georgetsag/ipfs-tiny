/* SPDX-License-Identifier: GPL-3.0-or-later */

#include <cppunit/TestCase.h>
#include <random>

#include "ipfs-tiny/ipld/dag_pb.hpp"
#include "ipfs-tiny/protobuf/rbuffer.hpp"
#include "ipfs-tiny/protobuf/wbuffer.hpp"
#include "test_dag_pb.hpp"
#include <etl/vector.h>
#include <random>

void
test_dag_pb::test_encode_decode()
{
  std::random_device                     rd;
  std::mt19937                           mt(rd());
  std::uniform_int_distribution<uint8_t> uni(0, 255);
  PBNode<16, 256 / 8, 32, 1024>          x;
  PBNode<16, 256 / 8, 32, 1024>          y;

  etl::vector<uint8_t, 1024> v;
  for (size_t i = 0; i < 1024; i++) {
    v.push_back(uni(mt));
  }
  x.mutable_Data().set(v.data(), v.size());
  ipfs_tiny::protobuf::rbuffer<2048> rb;
  ipfs_tiny::protobuf::wbuffer<2048> wb;

  EmbeddedProto::Error ret = x.serialize(wb);
  CPPUNIT_ASSERT(ret == EmbeddedProto::Error::NO_ERRORS);

  for (size_t i = 0; i < wb.get_size(); i++) {
    rb.push(wb[i]);
  }

  CPPUNIT_ASSERT(wb.get_size() == rb.get_size());
  ret = y.deserialize(rb);
  CPPUNIT_ASSERT(ret == EmbeddedProto::Error::NO_ERRORS);

  /* Data should match! */
  CPPUNIT_ASSERT(
      std::equal(x.get_Data().get_const(),
                 x.get_Data().get_const() + x.get_Data().get_length(),
                 y.get_Data().get_const()));
}
