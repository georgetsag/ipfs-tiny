/*
 * test_multihash.cpp
 *
 *  Created on: Mar 28, 2021
 *      Author: surligas
 */

#include "test_multihash.hpp"
#include "ipfs-tiny/crypto/sha256.hpp"
#include "ipfs-tiny/multiformats.hpp"

#include <iomanip>
#include <sstream>

void
test_multihash::multiformat_correct()
{
  etl::vector<uint8_t, 256 / 8 + 9 + 9 + 24> res;
  etl::vector<uint8_t, 32>                   msg;
  const std::string                          s = "Merkle–Damgård";
  const std::string                          end_res =
      "122041dd7b6443542e75701aa98a0c235951a28a0d851b11564d20022ab11d2589a8";

  for (char i : s) {
    msg.push_back(i);
  }

  ipfs_tiny::crypto::sha256 h0 =
      ipfs_tiny::crypto::sha256<etl::ivector<uint8_t>>();
  ipfs_tiny::multiformats::multihash<etl::ivector<uint8_t>>(h0, res, msg);

  std::stringstream stream;

  for (uint8_t i : res) {
    stream << std::hex << std::setw(2) << std::setfill('0')
           << static_cast<uint32_t>(i);
  }
  CPPUNIT_ASSERT(stream.str() == end_res);
}

void
test_multihash::multiformat_incorrect()
{
  etl::vector<uint8_t, 256 / 8 + 9 + 9 + 24> res;
  etl::vector<uint8_t, 32>                   msg;
  // Small change with the previous
  const std::string s = "Merkle–Damgårc";
  const std::string end_res =
      "122041dd7b6443542e75701aa98a0c235951a28a0d851b11564d20022ab11d2589a8";

  for (char i : s) {
    msg.push_back(i);
  }

  ipfs_tiny::crypto::sha256 h0 =
      ipfs_tiny::crypto::sha256<etl::ivector<uint8_t>>();
  ipfs_tiny::multiformats::multihash<etl::ivector<uint8_t>>(h0, res, msg);

  std::stringstream stream;

  for (uint8_t i : res) {
    stream << std::hex << std::setw(2) << std::setfill('0')
           << static_cast<uint32_t>(i);
  }
  CPPUNIT_ASSERT(stream.str() != end_res);
}
