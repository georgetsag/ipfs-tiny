

#ifndef TEST_X86_TEST_DAG_PB_HPP_
#define TEST_X86_TEST_DAG_PB_HPP_

#include <cppunit/extensions/HelperMacros.h>

class test_dag_pb : public CppUnit::TestFixture
{
  CPPUNIT_TEST_SUITE(test_dag_pb);
  CPPUNIT_TEST(test_encode_decode);
  CPPUNIT_TEST_SUITE_END();

public:
  void
  test_encode_decode();
};

#endif /* TEST_X86_TEST_DAG_PB_HPP_ */
