/* SPDX-License-Identifier: GPL-3.0-or-later */

#ifndef TEST_X86_TEST_BASE16_HPP_
#define TEST_X86_TEST_BASE16_HPP_

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

class test_base16 : public CppUnit::TestFixture
{
  CPPUNIT_TEST_SUITE(test_base16);
  CPPUNIT_TEST(t0);
  CPPUNIT_TEST(t1);
  CPPUNIT_TEST(t2);
  CPPUNIT_TEST_SUITE_END();

public:
  void
  t0();

  void
  t1();

  void
  t2();
};

#endif /* TEST_X86_TEST_BASE16_HPP_ */
