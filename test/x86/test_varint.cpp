/* SPDX-License-Identifier: GPL-3.0-or-later */

#include <cppunit/TestCase.h>
#include <random>

#include "ipfs-tiny/multiformats.hpp"
#include "test_varint.hpp"

void
random(size_t bits)
{
  std::random_device                      rd;
  std::mt19937                            mt(rd());
  const uint64_t                          m = (1UL << bits) - 1;
  std::uniform_int_distribution<uint64_t> uni(0, m);

  /* Test with etl::vector::append() */
  for (size_t i = 0; i < 10000; i++) {
    etl::vector<uint8_t, 9> v;
    uint64_t                x = uni(mt);
    ipfs_tiny::multiformats::varint::encode(v, x);
    uint64_t res;
    auto     iter =
        ipfs_tiny::multiformats::varint::decode(res, v.cbegin(), v.cend());
    CPPUNIT_ASSERT(x == res);
    CPPUNIT_ASSERT(iter != v.cbegin());
  }
}

void
test_varint::random_63()
{
  random(63);
}

void
test_varint::random_47()
{
  random(47);
}

void
test_varint::random_31()
{
  random(31);
}

void
test_varint::random_23()
{
  random(23);
}

void
test_varint::random_15()
{
  random(15);
}

void
test_varint::random_7()
{
  random(7);
}

void
test_varint::back_to_back()
{
  std::random_device                      rd;
  std::mt19937                            mt(rd());
  std::uniform_int_distribution<uint64_t> uni(0, (1UL << 63) - 1);

  /* Test 4 back to back varints, each one of random size */
  for (size_t i = 0; i < 10000; i++) {
    etl::vector<uint8_t, 4 * 9> v;
    const uint64_t              n0 = uni(mt);
    const uint64_t              n1 = uni(mt);
    const uint64_t              n2 = uni(mt);
    const uint64_t              n3 = uni(mt);

    ipfs_tiny::multiformats::varint::encode(v, n0);
    ipfs_tiny::multiformats::varint::encode(v, n1);
    ipfs_tiny::multiformats::varint::encode(v, n2);
    ipfs_tiny::multiformats::varint::encode(v, n3);

    uint64_t res;
    auto     iter =
        ipfs_tiny::multiformats::varint::decode(res, v.cbegin(), v.cend());
    CPPUNIT_ASSERT(n0 == res);
    CPPUNIT_ASSERT(iter != v.cbegin() && iter != v.cend());

    iter = ipfs_tiny::multiformats::varint::decode(res, iter, v.cend());
    CPPUNIT_ASSERT(n1 == res);
    CPPUNIT_ASSERT(iter != v.cbegin() && iter != v.cend());

    iter = ipfs_tiny::multiformats::varint::decode(res, iter, v.cend());
    CPPUNIT_ASSERT(n2 == res);
    CPPUNIT_ASSERT(iter != v.cbegin() && iter != v.cend());

    iter = ipfs_tiny::multiformats::varint::decode(res, iter, v.cend());
    CPPUNIT_ASSERT(n3 == res);
    /* We should have reaches the end of the vector */
    CPPUNIT_ASSERT(iter != v.cbegin() && iter == v.cend());
  }
}

void
test_varint::invalid()
{
  /* Test that the decode raises an exception if an invalid varint is given */
  etl::vector<uint8_t, 4 * 9> v;
  v.resize(4 * 9, 0xFF);

  uint64_t res;
  CPPUNIT_ASSERT_THROW(
      ipfs_tiny::multiformats::varint::decode(res, v.cbegin(), v.cend()),
      ipfs_tiny::multiformats::varint_invalid);
}
