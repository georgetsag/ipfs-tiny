/* SPDX-License-Identifier: GPL-3.0-or-later */

#ifndef TEST_X86_TEST_CID_H_
#define TEST_X86_TEST_CID_H_

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

class test_cid : public CppUnit::TestFixture
{
  CPPUNIT_TEST_SUITE(test_cid);
  CPPUNIT_TEST(basic_encoding);
  CPPUNIT_TEST_SUITE_END();

public:
  void
  basic_encoding();
};

#endif /* TEST_X86_TEST_CID_H_ */
