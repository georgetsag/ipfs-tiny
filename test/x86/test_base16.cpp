/* SPDX-License-Identifier: GPL-3.0-or-later */

#include "test_base16.hpp"
#include "ipfs-tiny/multiformats/base16.hpp"
#include <random>

void
test_base16::t0()
{
  ipfs_tiny::multiformats::base16<etl::ivector<uint8_t>, etl::istring> b;
  CPPUNIT_ASSERT(b.decode_size(16) == 8);
  CPPUNIT_ASSERT(b.encode_size(16) == 32);
  CPPUNIT_ASSERT_THROW(b.decode_size(15), ipfs_tiny::exception);
}

void
test_base16::t1()
{
  std::random_device                     rd;
  std::mt19937                           mt(rd());
  std::uniform_int_distribution<uint8_t> uni(0, 255);

  const size_t msg_len = 1024;
  ipfs_tiny::multiformats::base16<etl::ivector<uint8_t>, etl::istring> b;
  etl::vector<uint8_t, msg_len>                                        v0;
  etl::vector<uint8_t, msg_len>                                        v1;
  etl::string<msg_len * 2>                                             res;

  for (size_t i = 0; i < msg_len; i++) {
    v0.push_back(uni(mt));
  }

  b.encode(res, v0);
  b.decode(v1, res);
  CPPUNIT_ASSERT(etl::equal(v0.cbegin(), v0.cend(), v1.cbegin()));
}

void
test_base16::t2()
{
  std::random_device                     rd;
  std::mt19937                           mt(rd());
  std::uniform_int_distribution<uint8_t> uni(0, 255);

  const size_t msg_len = 1024;
  ipfs_tiny::multiformats::base16<etl::ivector<uint8_t>, etl::istring> b;
  etl::vector<uint8_t, msg_len>                                        v0;
  etl::vector<uint8_t, msg_len>                                        v1;
  etl::string<msg_len * 2>                                             res;

  for (size_t i = 0; i < msg_len; i++) {
    v0.push_back(uni(mt));
  }

  b.encode(res, v0);
  b.decode(v1, res.cbegin(), res.cend());
  CPPUNIT_ASSERT(v1 == v0);
}
