/* SPDX-License-Identifier: GPL-3.0-or-later */

#ifndef INCLUDE_IPFS_TINY_EXCEPTION_HPP_
#define INCLUDE_IPFS_TINY_EXCEPTION_HPP_

#include "etl/string.h"
#include "ipfs-tiny/config.h"
#include <exception>

namespace ipfs_tiny
{

class exception : public std::exception
{
public:
  static const size_t max_msg_size = IPFS_EXCEPT_MAX_MSG_LEN;
  exception(const char *reason, const char *file, size_t line);

  exception(const char *reason);
  const char *
  what() noexcept;

private:
  const char *m_reason;
#if IPFS_EXCEPT_VERBOSE == 1
  const bool                m_verbose;
  const char *              m_file;
  const size_t              m_line;
  etl::string<max_msg_size> m_str;
#endif
};

} // namespace ipfs_tiny

#endif /* INCLUDE_IPFS_TINY_EXCEPTION_HPP_ */
