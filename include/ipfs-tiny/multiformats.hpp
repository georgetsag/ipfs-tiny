/* SPDX-License-Identifier: GPL-3.0-or-later */

#ifndef INCLUDE_IPFS_TINY_MULTIFORMATS_HPP_
#define INCLUDE_IPFS_TINY_MULTIFORMATS_HPP_

#include "multiformats/multibase.hpp"
#include "multiformats/multicodec.hpp"
#include "multiformats/multihash.hpp"
#include "multiformats/varint.hpp"

#endif /* INCLUDE_IPFS_TINY_MULTIFORMATS_HPP_ */
