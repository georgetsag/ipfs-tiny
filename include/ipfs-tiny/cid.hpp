/* SPDX-License-Identifier: GPL-3.0-or-later */

#ifndef INCLUDE_IPFS_TINY_CID_HPP_
#define INCLUDE_IPFS_TINY_CID_HPP_

#include "etl/vector.h"
#include "ipfs-tiny/exception.hpp"
#include "ipfs-tiny/multiformats.hpp"

namespace ipfs_tiny
{

enum class cid_version
{
  v0,
  v1
};

class cid_parse_exception : public exception
{
public:
  cid_parse_exception(const char *file, size_t line)
      : exception("cid: Malformed CID", file, line)
  {
  }
};

template <const size_t MAX_SIZE, typename HASH = void> class cid
{
public:
  cid(const etl::ivector<uint8_t> &data)
      : m_version(cid_version::v1),
        m_multicodec(),
        m_code(multiformats::multicodec::raw)
  {
    /* Estimate the CID version*/
    if (data.size() == 46 && data[0] == 'Q' && data[1] == 'm') {
      m_version = cid_version::v0;
      m_code    = multiformats::multicodec::dag_pb;
    } else {
    }
  }

  cid(const etl::istring &data)
      : m_version(cid_version::v1),
        m_multicodec(),
        m_code(multiformats::multicodec::raw)
  {
    /* Estimate the CID version*/
    if (data.length() == 46 && data.compare(0, 2, "Qm") == 0) {
      m_version = cid_version::v0;
      m_code    = multiformats::multicodec::dag_pb;
    }
  }
  template <typename InputIterator>
  cid(InputIterator first, InputIterator last)
      : m_version(cid_version::v1),
        m_multicodec(),
        m_code(multiformats::multicodec::raw)
  {
    if (last - first == 46 && *first == 'Q' && *(first + 1) == 'm') {
      m_version = cid_version::v0;
      m_code    = multiformats::multicodec::dag_pb;
    }
  }

  cid_version
  version() const
  {
    return m_version;
  }

  friend bool
  operator==(const cid &lhs, const cid &rhs)
  {
    return lhs.version() == rhs.version() && lhs.m_buffer == rhs.m_buffer;
  }

  friend bool
  operator!=(const cid &lhs, const cid &rhs)
  {
    return !(lhs == rhs);
  }

private:
  cid_version                    m_version;
  etl::vector<uint8_t, MAX_SIZE> m_buffer;
  multiformats::multicodec       m_multicodec;
  multiformats::multicodec::code m_code;

  template <typename InputIterator>
  void
  parse(InputIterator first, InputIterator last)
  {
    InputIterator iter = first;
    /* Get codec */
    iter = m_multicodec.decode(iter);
    /* Get CID version */
    uint64_t res;
    iter =
        multiformats::varint(res, iter, iter + multiformats::varint::max_len);
  }
};

} /* namespace ipfs_tiny */

#endif /* INCLUDE_IPFS_TINY_CID_HPP_ */
