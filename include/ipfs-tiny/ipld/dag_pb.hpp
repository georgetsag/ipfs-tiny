/*
 *  Copyright (C) 2020-2021 Embedded AMS B.V. - All Rights Reserved
 *
 *  This file is part of Embedded Proto.
 *
 *  Embedded Proto is open source software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as published
 *  by the Free Software Foundation, version 3 of the license.
 *
 *  Embedded Proto  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Embedded Proto. If not, see <https://www.gnu.org/licenses/>.
 *
 *  For commercial and closed source application please visit:
 *  <https://EmbeddedProto.com/license/>.
 *
 *  Embedded AMS B.V.
 *  Info:
 *    info at EmbeddedProto dot com
 *
 *  Postal address:
 *    Johan Huizingalaan 763a
 *    1066 VH, Amsterdam
 *    the Netherlands
 */

// This file is generated. Please do not edit!
#ifndef DAG_PB_HPP
#define DAG_PB_HPP

#include <Errors.h>
#include <FieldStringBytes.h>
#include <Fields.h>
#include <MessageInterface.h>
#include <MessageSizeCalculator.h>
#include <ReadBufferSection.h>
#include <RepeatedFieldFixedSize.h>
#include <WireFormatter.h>
#include <cstdint>
#include <limits>

// Include external proto definitions

template <uint32_t Hash_LENGTH, uint32_t Name_LENGTH>
class PBLink final : public ::EmbeddedProto::MessageInterface
{
public:
  PBLink() = default;
  PBLink(const PBLink &rhs)
  {
    set_Hash(rhs.get_Hash());
    set_Name(rhs.get_Name());
    set_Tsize(rhs.get_Tsize());
  }

  PBLink(const PBLink &&rhs) noexcept
  {
    set_Hash(rhs.get_Hash());
    set_Name(rhs.get_Name());
    set_Tsize(rhs.get_Tsize());
  }

  ~PBLink() override = default;

  enum class id : uint32_t
  {
    NOT_SET = 0,
    HASH    = 1,
    NAME    = 2,
    TSIZE   = 3
  };

  PBLink &
  operator=(const PBLink &rhs)
  {
    set_Hash(rhs.get_Hash());
    set_Name(rhs.get_Name());
    set_Tsize(rhs.get_Tsize());
    return *this;
  }

  PBLink &
  operator=(const PBLink &&rhs) noexcept
  {
    set_Hash(rhs.get_Hash());
    set_Name(rhs.get_Name());
    set_Tsize(rhs.get_Tsize());
    return *this;
  }

  inline void
  clear_Hash()
  {
    Hash_.clear();
  }
  inline ::EmbeddedProto::FieldBytes<Hash_LENGTH> &
  mutable_Hash()
  {
    return Hash_;
  }
  inline void
  set_Hash(const ::EmbeddedProto::FieldBytes<Hash_LENGTH> &rhs)
  {
    Hash_.set(rhs);
  }
  inline const ::EmbeddedProto::FieldBytes<Hash_LENGTH> &
  get_Hash() const
  {
    return Hash_;
  }
  inline const uint8_t *
  Hash() const
  {
    return Hash_.get_const();
  }

  inline void
  clear_Name()
  {
    Name_.clear();
  }
  inline ::EmbeddedProto::FieldString<Name_LENGTH> &
  mutable_Name()
  {
    return Name_;
  }
  inline void
  set_Name(const ::EmbeddedProto::FieldString<Name_LENGTH> &rhs)
  {
    Name_.set(rhs);
  }
  inline const ::EmbeddedProto::FieldString<Name_LENGTH> &
  get_Name() const
  {
    return Name_;
  }
  inline const char *
  Name() const
  {
    return Name_.get_const();
  }

  inline void
  clear_Tsize()
  {
    Tsize_.clear();
  }
  inline void
  set_Tsize(const EmbeddedProto::uint64 &value)
  {
    Tsize_ = value;
  }
  inline void
  set_Tsize(const EmbeddedProto::uint64 &&value)
  {
    Tsize_ = value;
  }
  inline EmbeddedProto::uint64 &
  mutable_Tsize()
  {
    return Tsize_;
  }
  inline const EmbeddedProto::uint64 &
  get_Tsize() const
  {
    return Tsize_;
  }
  inline EmbeddedProto::uint64::FIELD_TYPE
  Tsize() const
  {
    return Tsize_.get();
  }

  ::EmbeddedProto::Error
  serialize(::EmbeddedProto::WriteBufferInterface &buffer) const override
  {
    ::EmbeddedProto::Error return_value = ::EmbeddedProto::Error::NO_ERRORS;

    if (::EmbeddedProto::Error::NO_ERRORS == return_value) {
      return_value = Hash_.serialize_with_id(static_cast<uint32_t>(id::HASH),
                                             buffer, false);
    }

    if (::EmbeddedProto::Error::NO_ERRORS == return_value) {
      return_value = Name_.serialize_with_id(static_cast<uint32_t>(id::NAME),
                                             buffer, false);
    }

    if ((0U != Tsize_.get()) &&
        (::EmbeddedProto::Error::NO_ERRORS == return_value)) {
      return_value = Tsize_.serialize_with_id(static_cast<uint32_t>(id::TSIZE),
                                              buffer, false);
    }

    return return_value;
  };

  ::EmbeddedProto::Error
  deserialize(::EmbeddedProto::ReadBufferInterface &buffer) override
  {
    ::EmbeddedProto::Error return_value = ::EmbeddedProto::Error::NO_ERRORS;
    ::EmbeddedProto::WireFormatter::WireType wire_type =
        ::EmbeddedProto::WireFormatter::WireType::VARINT;
    uint32_t id_number = 0;
    id       id_tag    = id::NOT_SET;

    ::EmbeddedProto::Error tag_value =
        ::EmbeddedProto::WireFormatter::DeserializeTag(buffer, wire_type,
                                                       id_number);
    while ((::EmbeddedProto::Error::NO_ERRORS == return_value) &&
           (::EmbeddedProto::Error::NO_ERRORS == tag_value)) {
      id_tag = static_cast<id>(id_number);
      switch (id_tag) {
      case id::HASH:
        return_value = Hash_.deserialize_check_type(buffer, wire_type);
        break;

      case id::NAME:
        return_value = Name_.deserialize_check_type(buffer, wire_type);
        break;

      case id::TSIZE:
        return_value = Tsize_.deserialize_check_type(buffer, wire_type);
        break;

      default:
        break;
      }

      if (::EmbeddedProto::Error::NO_ERRORS == return_value) {
        // Read the next tag.
        tag_value = ::EmbeddedProto::WireFormatter::DeserializeTag(
            buffer, wire_type, id_number);
      }
    }

    // When an error was detect while reading the tag but no other errors where
    // found, set it in the return value.
    if ((::EmbeddedProto::Error::NO_ERRORS == return_value) &&
        (::EmbeddedProto::Error::NO_ERRORS != tag_value) &&
        (::EmbeddedProto::Error::END_OF_BUFFER !=
         tag_value)) // The end of the buffer is not an array in this case.
    {
      return_value = tag_value;
    }

    return return_value;
  };

  void
  clear() override
  {
    clear_Hash();
    clear_Name();
    clear_Tsize();
  }

private:
  ::EmbeddedProto::FieldBytes<Hash_LENGTH>  Hash_;
  ::EmbeddedProto::FieldString<Name_LENGTH> Name_;
  EmbeddedProto::uint64                     Tsize_ = 0U;
};

template <uint32_t Links_REP_LENGTH, uint32_t Links_Hash_LENGTH,
          uint32_t Links_Name_LENGTH, uint32_t Data_LENGTH>
class PBNode final : public ::EmbeddedProto::MessageInterface
{
public:
  PBNode() = default;
  PBNode(const PBNode &rhs)
  {
    set_Links(rhs.get_Links());
    set_Data(rhs.get_Data());
  }

  PBNode(const PBNode &&rhs) noexcept
  {
    set_Links(rhs.get_Links());
    set_Data(rhs.get_Data());
  }

  ~PBNode() override = default;

  enum class id : uint32_t
  {
    NOT_SET = 0,
    DATA    = 1,
    LINKS   = 2
  };

  PBNode &
  operator=(const PBNode &rhs)
  {
    set_Links(rhs.get_Links());
    set_Data(rhs.get_Data());
    return *this;
  }

  PBNode &
  operator=(const PBNode &&rhs) noexcept
  {
    set_Links(rhs.get_Links());
    set_Data(rhs.get_Data());
    return *this;
  }

  inline const PBLink<Links_Hash_LENGTH, Links_Name_LENGTH> &
  Links(uint32_t index) const
  {
    return Links_[index];
  }
  inline void
  clear_Links()
  {
    Links_.clear();
  }
  inline void
  set_Links(uint32_t                                            index,
            const PBLink<Links_Hash_LENGTH, Links_Name_LENGTH> &value)
  {
    Links_.set(index, value);
  }
  inline void
  set_Links(uint32_t                                             index,
            const PBLink<Links_Hash_LENGTH, Links_Name_LENGTH> &&value)
  {
    Links_.set(index, value);
  }
  inline void
  set_Links(const ::EmbeddedProto::RepeatedFieldFixedSize<
            PBLink<Links_Hash_LENGTH, Links_Name_LENGTH>, Links_REP_LENGTH>
                &values)
  {
    Links_ = values;
  }
  inline void
  add_Links(const PBLink<Links_Hash_LENGTH, Links_Name_LENGTH> &value)
  {
    Links_.add(value);
  }
  inline ::EmbeddedProto::RepeatedFieldFixedSize<
      PBLink<Links_Hash_LENGTH, Links_Name_LENGTH>, Links_REP_LENGTH> &
  mutable_Links()
  {
    return Links_;
  }
  inline PBLink<Links_Hash_LENGTH, Links_Name_LENGTH> &
  mutable_Links(uint32_t index)
  {
    return Links_[index];
  }
  inline const ::EmbeddedProto::RepeatedFieldFixedSize<
      PBLink<Links_Hash_LENGTH, Links_Name_LENGTH>, Links_REP_LENGTH> &
  get_Links() const
  {
    return Links_;
  }
  inline const ::EmbeddedProto::RepeatedFieldFixedSize<
      PBLink<Links_Hash_LENGTH, Links_Name_LENGTH>, Links_REP_LENGTH> &
  Links() const
  {
    return Links_;
  }

  inline void
  clear_Data()
  {
    Data_.clear();
  }
  inline ::EmbeddedProto::FieldBytes<Data_LENGTH> &
  mutable_Data()
  {
    return Data_;
  }
  inline void
  set_Data(const ::EmbeddedProto::FieldBytes<Data_LENGTH> &rhs)
  {
    Data_.set(rhs);
  }
  inline const ::EmbeddedProto::FieldBytes<Data_LENGTH> &
  get_Data() const
  {
    return Data_;
  }
  inline const uint8_t *
  Data() const
  {
    return Data_.get_const();
  }

  ::EmbeddedProto::Error
  serialize(::EmbeddedProto::WriteBufferInterface &buffer) const override
  {
    ::EmbeddedProto::Error return_value = ::EmbeddedProto::Error::NO_ERRORS;

    if (::EmbeddedProto::Error::NO_ERRORS == return_value) {
      return_value = Links_.serialize_with_id(static_cast<uint32_t>(id::LINKS),
                                              buffer, false);
    }

    if (::EmbeddedProto::Error::NO_ERRORS == return_value) {
      return_value = Data_.serialize_with_id(static_cast<uint32_t>(id::DATA),
                                             buffer, false);
    }

    return return_value;
  };

  ::EmbeddedProto::Error
  deserialize(::EmbeddedProto::ReadBufferInterface &buffer) override
  {
    ::EmbeddedProto::Error return_value = ::EmbeddedProto::Error::NO_ERRORS;
    ::EmbeddedProto::WireFormatter::WireType wire_type =
        ::EmbeddedProto::WireFormatter::WireType::VARINT;
    uint32_t id_number = 0;
    id       id_tag    = id::NOT_SET;

    ::EmbeddedProto::Error tag_value =
        ::EmbeddedProto::WireFormatter::DeserializeTag(buffer, wire_type,
                                                       id_number);
    while ((::EmbeddedProto::Error::NO_ERRORS == return_value) &&
           (::EmbeddedProto::Error::NO_ERRORS == tag_value)) {
      id_tag = static_cast<id>(id_number);
      switch (id_tag) {
      case id::LINKS:
        return_value = Links_.deserialize_check_type(buffer, wire_type);
        break;

      case id::DATA:
        return_value = Data_.deserialize_check_type(buffer, wire_type);
        break;

      default:
        break;
      }

      if (::EmbeddedProto::Error::NO_ERRORS == return_value) {
        // Read the next tag.
        tag_value = ::EmbeddedProto::WireFormatter::DeserializeTag(
            buffer, wire_type, id_number);
      }
    }

    // When an error was detect while reading the tag but no other errors where
    // found, set it in the return value.
    if ((::EmbeddedProto::Error::NO_ERRORS == return_value) &&
        (::EmbeddedProto::Error::NO_ERRORS != tag_value) &&
        (::EmbeddedProto::Error::END_OF_BUFFER !=
         tag_value)) // The end of the buffer is not an array in this case.
    {
      return_value = tag_value;
    }

    return return_value;
  };

  void
  clear() override
  {
    clear_Links();
    clear_Data();
  }

private:
  ::EmbeddedProto::RepeatedFieldFixedSize<
      PBLink<Links_Hash_LENGTH, Links_Name_LENGTH>, Links_REP_LENGTH>
                                           Links_;
  ::EmbeddedProto::FieldBytes<Data_LENGTH> Data_;
};

#endif // DAG_PB_HPP
