/*
 * multibase.hpp
 *
 *  Created on: Jun 10, 2021
 *      Author: surligas
 */

#ifndef INCLUDE_IPFS_TINY_MULTIFORMATS_MULTIBASE_HPP_
#define INCLUDE_IPFS_TINY_MULTIFORMATS_MULTIBASE_HPP_

#include "base.hpp"
#include "base16.hpp"
#include "base58.hpp"
#include "etl/map.h"
#include "ipfs-tiny/exception.hpp"
#include <type_traits>

namespace ipfs_tiny
{

namespace multiformats
{

namespace multibase
{

class uknown_base : public exception
{
public:
  uknown_base(const char *file, size_t line)
      : exception("multibase: Unknown base", file, line)
  {
  }
};

/**
 *
 */
enum class encoding : char
{
  extract   = 0,   /**< Search for the encoding. Valid only during decoding */
  base16    = 'f', /**< base16 */
  base58btc = 'z'  /**< base58btc */
};

template <encoding T, typename EncodedT, typename DecodedT>
static EncodedT &
encode(EncodedT &res, const DecodedT &data)
{
  if constexpr (T == encoding::base16) {
    base16<DecodedT, EncodedT> b16;
    etl::string<1>             c;
    c.push_back(b16.code());
    res.append(c);
    return b16.encode(res, data);
  } else if constexpr (T == encoding::base58btc) {
    base58<DecodedT, EncodedT> b58;
    etl::string<1>             c;
    c.push_back(b58.code());
    res.append(c);
    return b58.encode(res, data);
  } else {
    throw uknown_base(__FILE__, __LINE__);
  }
}

template <typename DecodedT, typename EncodedT>
static DecodedT &
decode(DecodedT &res, typename EncodedT::const_iterator first,
       typename EncodedT::const_iterator last)
{
  if (first == last) {
    return res;
  }

  switch (static_cast<encoding>(*first++)) {
  case encoding::base16: {
    ipfs_tiny::multiformats::base16<DecodedT, EncodedT> b;
    b.decode(res, first, last);
    return res;
  }
  case encoding::base58btc: {
    ipfs_tiny::multiformats::base58<DecodedT, EncodedT> b;
    b.decode(res, first, last);
    return res;
  }
  default:
    throw uknown_base(__FILE__, __LINE__);
  }
}
} // namespace multibase
// namespace multibase

} // namespace multiformats

} // namespace ipfs_tiny

#endif /* INCLUDE_IPFS_TINY_MULTIFORMATS_MULTIBASE_HPP_ */
