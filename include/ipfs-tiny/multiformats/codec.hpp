/*
 * codec.hpp
 *
 *  Created on: Jun 11, 2021
 *      Author: surligas
 */

#ifndef INCLUDE_IPFS_TINY_MULTIFORMATS_CODEC_HPP_
#define INCLUDE_IPFS_TINY_MULTIFORMATS_CODEC_HPP_

#include <etl/string.h>

namespace ipfs_tiny
{

namespace multiformats
{

class codec
{
public:
  codec(const etl::istring &name, uint32_t code);

  ~codec() {}

  uint32_t
  code() const;

  const etl::istring &
  name() const;

  friend bool
  operator==(const codec &lhs, const codec &rhs);

private:
  const etl::string<32> m_name;
  const uint32_t        m_code;
};

} // namespace multiformats

} // namespace ipfs_tiny

#endif /* INCLUDE_IPFS_TINY_MULTIFORMATS_CODEC_HPP_ */
