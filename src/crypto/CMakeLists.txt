# SPDX-License-Identifier: GPL-3.0-or-later

set(CRYPTO_SOURCES
    sha256.cpp)

add_library(ipfs-tiny-crypto SHARED ${CRYPTO_SOURCES})
target_include_directories(ipfs-tiny-crypto PUBLIC
                          "${PROJECT_BINARY_DIR}/etl/include"
                          "${PROJECT_SOURCE_DIR}/etl/include"
                          "${PROJECT_BINARY_DIR}/include"
                          "${PROJECT_SOURCE_DIR}/include"
                          )