# SPDX-License-Identifier: GPL-3.0-or-later

set(MULTIFORMATS_SOURCES
    codec.cpp)

add_library(ipfs-tiny-multiformats SHARED ${MULTIFORMATS_SOURCES})
target_include_directories(ipfs-tiny-multiformats PUBLIC
                          "${PROJECT_BINARY_DIR}/etl/include"
                          "${PROJECT_SOURCE_DIR}/etl/include"
                          "${PROJECT_BINARY_DIR}/include"
                          "${PROJECT_SOURCE_DIR}/include"
                          )