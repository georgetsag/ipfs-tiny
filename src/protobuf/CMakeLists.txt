# SPDX-License-Identifier: GPL-3.0-or-later

set(PROTOBUF_SOURCES
    "${PROJECT_SOURCE_DIR}/EmbeddedProto/src/Fields.cpp"
    "${PROJECT_SOURCE_DIR}/EmbeddedProto/src/MessageInterface.cpp"
    "${PROJECT_SOURCE_DIR}/EmbeddedProto/src/ReadBufferSection.cpp")

add_library(ipfs-tiny-protobuf SHARED ${PROTOBUF_SOURCES})
target_include_directories(ipfs-tiny-protobuf PUBLIC
                          "${PROJECT_BINARY_DIR}/EmbeddedProto/src"
                          "${PROJECT_SOURCE_DIR}/EmbeddedProto/src"
                          )